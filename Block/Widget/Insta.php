<?php

namespace NStudios\InstagramPost\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Insta extends Template implements BlockInterface
{

    protected $_template = "NStudios_InstagramPost::widget/insta.phtml";

}