 define([
    'jquery',
    'NStudios_InstagramPost/js/bootstrap.min',
    'NStudios_InstagramPost/js/instaCustom',
  ],function($,config){
      $( document ).ready(function() {
        $('.carousel').carousel({
          interval: false
        });
      });

    return function (config) {
        var token = config.options,
        num_photos = 20, // maximum 20
        container = document.getElementById( 'squishmallow' ), 
        scrElement = document.createElement( 'script' ),
        profileImg = document.getElementById('insta-btn'),
        user= document.getElementById('insta-user'),
        indicators = document.getElementById('indicators');
        window.mishaProcessResult = function( data ) {
            profileImg.innerHTML += '<img id="profile" src="' + data.data[1].user.profile_picture + '">';
            // user.innerHTML += data.data[1].user.username;
        	for( x in data.data ){
                let counter = 0;   
                // console.log(data.data[x].videos.standard_resolution.url) ;
                if (data.data[x].videos) {
                  container.innerHTML+='<div class="carousel-item"><video class="insta-video"  class="embed-responsive-item"  controls><source src="'+ data.data[x].videos.standard_resolution.url +'"></video></div>';
                }else{
                  container.innerHTML += '<div class="carousel-item"><img src="' + data.data[x].images.standard_resolution.url + '"></div>';
                }
                
                indicators.innerHTML +='<li data-target="#carouselExampleIndicators" class="exindicator" data-slide-to="' + counter + '"></li>' ; 
                counter ++;
              }
            
                let getTags= document.getElementsByClassName('carousel-item');
                let indicator = document.getElementsByClassName('exindicator'); 
                for (let i = 0; i < getTags.length; i++) {
                    const element = getTags[i];
                    indicator[0].classList.add('active');
                    if (data.data[x].videos) { 
                      
                      }
                      getTags[0].classList.add('active');
                      if (data.data[i].caption) {
                          element.innerHTML += '<div class="insta-tags"><p>'+ data.data[i].caption.text+'</p></div>';
                    }
                }
            }
        scrElement.setAttribute( 'src', 'https://api.instagram.com/v1/users/self/media/recent?access_token=' + token + '&count=' + num_photos + '&callback=mishaProcessResult' );
        document.body.appendChild( scrElement );

        
        
            
        }
        
  }
  
  );
